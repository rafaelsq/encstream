package encstream

import (
	"crypto/aes"
	"crypto/cipher"
	"io"
)

type Decoder interface {
	SetIVLength(int)
	SetSaltLength(int)
	SetIterations(int)
	SetKeyLength(int)
	Decode(io.Reader, []byte) error
}

func NewDecoder(w io.Writer) Decoder {
	return &Dec{w: w}
}

type Dec struct {
	w io.Writer

	iterations int
	keyLength  int
	saltLength int
	ivLength   int
}

func (d *Dec) SetSaltLength(length int) {
	d.saltLength = length
}

func (d *Dec) SetIVLength(length int) {
	d.ivLength = length
}

func (d *Dec) SetIterations(iterations int) {
	d.iterations = iterations
}

func (d *Dec) SetKeyLength(keyLength int) {
	d.keyLength = keyLength
}

func (d *Dec) Decode(in io.Reader, pass []byte) error {
	salt := make([]byte, d.saltLength)
	_, err := in.Read(salt)
	if err != nil {
		return err
	}

	iv := make([]byte, d.ivLength)
	_, err = in.Read(iv)
	if err != nil {
		return err
	}

	derivedKey := NewDerivedKey(pass, salt, d.iterations, d.keyLength)

	b, err := aes.NewCipher(derivedKey)
	if err != nil {
		return err
	}

	aesgcm, err := cipher.NewGCM(b)
	if err != nil {
		return err
	}

	for {
		chunk := make([]byte, chunkSize+16)

		if n, err := in.Read(chunk); err == io.EOF {
			break
		} else if err != nil {
			return err
		} else {
			data, err := aesgcm.Open(nil, iv, chunk[:n], nil)
			if err != nil {
				return err
			}

			_, err = d.w.Write(data)
			if err != nil {
				return err
			}
		}
	}
	return nil
}
