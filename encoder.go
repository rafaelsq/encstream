package encstream

import (
	"crypto/aes"
	"crypto/cipher"
	"fmt"
	"io"
)

type Encoder interface {
	SetSalt([]byte)
	GetSalt() []byte
	SetIV([]byte)
	SetDerivedKey([]byte)
	Encode(io.Reader) error
}

func NewEncoder(w io.Writer) Encoder {
	return &Enc{w: w}
}

type Enc struct {
	w io.Writer

	salt       []byte
	iv         []byte
	derivedKey []byte
}

func (e *Enc) SetSalt(salt []byte) {
	e.salt = salt
}

func (e *Enc) GetSalt() []byte {
	return e.salt
}

func (e *Enc) SetIV(iv []byte) {
	e.iv = iv
}

func (e *Enc) SetDerivedKey(derivedKey []byte) {
	e.derivedKey = derivedKey
}

func (e *Enc) Encode(in io.Reader) error {
	cypherBlock, err := aes.NewCipher(e.derivedKey)
	if err != nil {
		return fmt.Errorf("fail at new cypher block; %v", err)
	}

	aesgcm, err := cipher.NewGCM(cypherBlock)
	if err != nil {
		return fmt.Errorf("fail at new GCM; %v", err)
	}

	_, err = e.w.Write(e.salt)
	if err != nil {
		return err
	}

	_, err = e.w.Write(e.iv)
	if err != nil {
		return err
	}

	for {
		chunk := make([]byte, chunkSize)
		if n, err := in.Read(chunk); err == io.EOF {
			break
		} else if err != nil {
			return err
		} else {
			ecn := aesgcm.Seal(nil, e.iv, chunk[:n], nil)
			_, err := e.w.Write(ecn)
			if err != nil {
				return err
			}
		}
	}

	return nil
}
