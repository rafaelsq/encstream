package encstream

import (
	"crypto/rand"
	"crypto/sha256"

	"golang.org/x/crypto/pbkdf2"
)

const chunkSize = 2 << 10

func NewDerivedKey(passphrase, salt []byte, iterations, keyLength int) []byte {
	return pbkdf2.Key(passphrase, salt, iterations, keyLength, sha256.New)
}

func NewSalt(saltLength int) []byte {
	salt := make([]byte, saltLength)
	// http://www.ietf.org/rfc/rfc2898.txt
	rand.Read(salt)
	return salt
}

func NewIV(ivLength int) []byte {
	// http://nvlpubs.nist.gov/nistpubs/Legacy/SP/nistspecialpublication800-38d.pdf
	// Section 8.2
	iv := make([]byte, ivLength)
	rand.Read(iv)
	return iv
}
