package encstream_test

import (
	"archive/tar"
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"strings"
	"testing"

	"github.com/rafaelsq/encstream"
)

const (
	iterations = 100000
	keyLength  = 32
	ivLength   = 12
	saltLength = 32
)

var pass = []byte("p4ssW0rd!")

func TestEnc(t *testing.T) {
	content := "Hello World!"

	r := strings.NewReader(content)
	w := bytes.NewBuffer([]byte{})

	salt := encstream.NewSalt(saltLength)

	e := encstream.NewEncoder(w)
	e.SetSalt(salt)
	e.SetIV(encstream.NewIV(ivLength))
	e.SetDerivedKey(encstream.NewDerivedKey(pass, salt, iterations, keyLength))
	if err := e.Encode(r); err != nil {
		t.Error(err)
	}

	w2 := bytes.NewBuffer([]byte{})
	d := encstream.NewDecoder(w2)
	d.SetIVLength(ivLength)
	d.SetSaltLength(saltLength)
	d.SetIterations(iterations)
	d.SetKeyLength(keyLength)
	if err := d.Decode(w, pass); err != nil {
		t.Error(err)
	}

	decoded := w2.String()
	if decoded != content {
		t.Errorf("decode string(%s) doesn't match \"%s\"\n", decoded, content)
	}
}

func TestStream(t *testing.T) {
	salt := encstream.NewSalt(saltLength)
	derivedKey := encstream.NewDerivedKey(pass, salt, iterations, keyLength)

	data := map[string]interface{}{
		"name": "stream",
	}

	r, w := io.Pipe()
	dr, dw := io.Pipe()
	jdr, jdw := io.Pipe()

	var newData map[string]interface{}

	je := json.NewEncoder(w)
	jd := json.NewDecoder(jdr)

	e := encstream.NewEncoder(dw)
	e.SetSalt(salt)
	e.SetIV(encstream.NewIV(ivLength))
	e.SetDerivedKey(derivedKey)

	d := encstream.NewDecoder(jdw)
	d.SetIVLength(ivLength)
	d.SetSaltLength(saltLength)
	d.SetIterations(iterations)
	d.SetKeyLength(keyLength)

	go func() {
		defer w.Close()
		if err := je.Encode(&data); err != nil {
			t.Error(err)
		}
	}()

	go func() {
		defer func() {
			r.Close()
			dw.Close()
		}()
		if err := e.Encode(r); err != nil {
			t.Error(err)
		}
	}()

	go func() {
		defer func() {
			dr.Close()
			jdw.Close()
		}()
		if err := d.Decode(dr, pass); err != nil {
			t.Error(err)
		}
	}()

	if err := jd.Decode(&newData); err != nil {
		t.Error(err)
	}
	jdr.Close()

	if newData["name"] != data["name"] {
		t.Error("decode data is different")
	}
}

func TestStreamTarArchive(t *testing.T) {
	salt := encstream.NewSalt(saltLength)

	r, w := io.Pipe()   // tar -> enc
	er, ew := io.Pipe() // enc -> dec
	dr, dw := io.Pipe() // dec -> tar

	e := encstream.NewEncoder(ew)
	e.SetSalt(salt)
	e.SetIV(encstream.NewIV(ivLength))
	e.SetDerivedKey(encstream.NewDerivedKey(pass, salt, iterations, keyLength))

	// tar -> enc
	go func() {
		defer w.Close()

		tw := tar.NewWriter(w)
		var files = []struct {
			Name, Body string
		}{
			{"readme.txt", "This archive contains some text files."},
			{"gopher.txt", "Gopher names:\nGeorge\nGeoffrey\nGonzo"},
			{"todo.txt", "Get animal handling license."},
		}
		for _, file := range files {
			hdr := &tar.Header{
				Name: file.Name,
				Mode: 0600,
				Size: int64(len(file.Body)),
			}
			if err := tw.WriteHeader(hdr); err != nil {
				t.Fatal(err)
			}
			if _, err := tw.Write([]byte(file.Body)); err != nil {
				t.Fatal(err)
			}
		}
		if err := tw.Close(); err != nil {
			log.Fatal(err)
		}
	}()

	// enc -> dec
	go func() {
		defer func() {
			r.Close()
			ew.Close()
		}()
		if err := e.Encode(r); err != nil {
			t.Error(err)
		}
	}()

	// dec -> tar
	go func() {
		defer func() {
			er.Close()
			dw.Close()
		}()

		d := encstream.NewDecoder(dw)
		d.SetIVLength(ivLength)
		d.SetSaltLength(saltLength)
		d.SetIterations(iterations)
		d.SetKeyLength(keyLength)
		if err := d.Decode(er, pass); err != nil {
			t.Fatal(err)
		}
	}()

	// tar -> print
	defer dr.Close()
	tr := tar.NewReader(dr)
	for {
		hdr, err := tr.Next()
		if err == io.EOF {
			break
		}

		if err != nil {
			t.Fatal(err)
		}

		fmt.Printf("Contents of %s:\n", hdr.Name)
		if _, err := io.Copy(os.Stdout, tr); err != nil {
			t.Fatal(err)
		}
		fmt.Println()
	}
}
